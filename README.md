Make sure to replace "CLOUD_NAME" and "API_KEY" in example html files with your cloudinary params that can be found on the cloudinary dashboard.

Then in cloudinary, make new upload preset (settings -> upload in cloudinary) called "test_preset" and make sure it is unsigned.

After that, install "http-server" (npm install http-server -g).

Next in the command line, go to the file directory containing the html files, and run "http-server" in the command line.

The command line will show which address the files are available on, enter any one into a browser window, and select the file you want to test.

After that, select the upload link, and try uploading different pdfs and images. The image will try to appear below if it is successful, and clicking the picture leads to the download link.